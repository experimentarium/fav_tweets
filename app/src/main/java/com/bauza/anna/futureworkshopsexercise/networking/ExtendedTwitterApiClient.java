package com.bauza.anna.futureworkshopsexercise.networking;

import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterSession;

/**
 * Created by shmoolca on 24.11.2016.
 */
public class ExtendedTwitterApiClient extends TwitterApiClient {
    public ExtendedTwitterApiClient(TwitterSession session) {
        super(session);
    }

    /**
     * Provide CustomService with defined endpoints
     */
    public CustomService getCustomService() {
        return getService(CustomService.class);
    }
}

