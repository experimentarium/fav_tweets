package com.bauza.anna.futureworkshopsexercise.networking;

import com.twitter.sdk.android.core.models.Tweet;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface CustomService {

    @POST("/1.1/favorites/create.json")
    Call<Tweet> favorite(@Query("id") long id);

    @POST("/1.1/favorites/destroy.json")
    Call<Tweet> unfavorite(@Query("id") long id);
}
