package com.bauza.anna.futureworkshopsexercise.views;

import android.databinding.ObservableArrayList;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bauza.anna.futureworkshopsexercise.BR;
import com.bauza.anna.futureworkshopsexercise.R;
import com.bauza.anna.futureworkshopsexercise.view_models.TweetViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shmoolca on 24.11.2016.
 */
public class TweetsAdapter extends  RecyclerView.Adapter<TweetViewHolder> {

    ArrayList<TweetViewModel> tweetViewModels;

    public TweetsAdapter()
    {
        this.tweetViewModels = new ArrayList<>();
    }

    public void reloadAll(List<TweetViewModel> tweetViewModels)
    {
        this.tweetViewModels.clear();
        this.tweetViewModels.addAll(tweetViewModels);
        notifyDataSetChanged();
    }

    @Override
    public TweetViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_view_tweet, parent, false);
        TweetViewHolder rcv = new TweetViewHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(TweetViewHolder holder, int position) {
         TweetViewModel tweetViewModel = tweetViewModels.get(position);
        holder.getBinding().setVariable(BR.tweetViewModel, tweetViewModel);
    }

    @Override
    public int getItemCount() {
        return this.tweetViewModels.size();
    }
}
