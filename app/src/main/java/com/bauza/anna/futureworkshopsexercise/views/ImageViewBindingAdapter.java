package com.bauza.anna.futureworkshopsexercise.views;

import android.databinding.BindingAdapter;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * Created by shmoolca on 24.11.2016.
 */
public class ImageViewBindingAdapter {

    public static int SCROLL_TAG = 123;

    @BindingAdapter("bind:imageUrl")
    public static void loadImage(final ImageView imageView, String lead_image_url) {
        Picasso.with(imageView.getContext())
                .load(lead_image_url)
                .tag(SCROLL_TAG)
                .into(imageView);
    }


    @BindingAdapter({"bind:src"})
    public static void setImageViewResource(ImageView imageView, int resource) {
        imageView.setImageResource(resource);
        Drawable drawable = imageView.getDrawable();
        if (drawable instanceof Animatable) {
            ((Animatable) drawable).start();
        }
    }

}
