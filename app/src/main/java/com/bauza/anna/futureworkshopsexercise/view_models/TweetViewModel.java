package com.bauza.anna.futureworkshopsexercise.view_models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.bauza.anna.futureworkshopsexercise.BR;
import com.bauza.anna.futureworkshopsexercise.R;
import com.twitter.sdk.android.core.models.Tweet;

import rx.functions.Func1;

/**
 * Created by shmoolca on 23.11.2016.
 */

public class TweetViewModel extends BaseObservable {

    private long tweet_id;
    private long user_id;
    private String profile_picture;
    private String text;
    private LoadingToggleViewModel favouriteToggleViewModel;

    public TweetViewModel(long tweet_id, String text , String profile_picture, long user_id,boolean favourite)
    {
        setTweet_id(tweet_id);
        setText(text);
        setProfile_picture(profile_picture);
        setUser_id(user_id);
        setFavouriteToggleViewModel(new LoadingToggleViewModel(favourite?LoadingToggleState.Selected:LoadingToggleState.Unselected));

        favouriteToggleViewModel.setDrawableForState(R.drawable.ic_favorite_green_stroke_24dp, LoadingToggleState.Unselected);
        favouriteToggleViewModel.setDrawableForState(R.drawable.ic_favorite_loading_24dp, LoadingToggleState.Loading);
        favouriteToggleViewModel.setDrawableForState(R.drawable.ic_favorite_green_24dp, LoadingToggleState.Selected);
    }

    public static final Func1<Tweet, TweetViewModel> MAPPER = new Func1<Tweet, TweetViewModel>() {
        @Override public TweetViewModel call(Tweet tweet) {
            return new TweetViewModel(tweet.id, tweet.text, tweet.user.profileImageUrl,tweet.user.getId(),tweet.favorited);
        }
    };

    @Bindable
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
        notifyPropertyChanged(BR.text);
    }

    @Bindable
    public String getProfile_picture() {
        return profile_picture;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
        notifyPropertyChanged(BR.profile_picture);
    }

    @Bindable
    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
        notifyPropertyChanged(BR.user_id);
    }

    @Bindable
    public long getTweet_id() {
        return tweet_id;
    }

    public void setTweet_id(long tweet_id) {
        this.tweet_id = tweet_id;
        notifyPropertyChanged(BR.tweet_id);
    }

    @Bindable
    public LoadingToggleViewModel getFavouriteToggleViewModel() {
        return favouriteToggleViewModel;
    }

    public void setFavouriteToggleViewModel(LoadingToggleViewModel favouriteToggleViewModel) {
        this.favouriteToggleViewModel = favouriteToggleViewModel;
        notifyPropertyChanged(BR.favouriteToggleViewModel);
    }

}
