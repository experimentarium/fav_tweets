package com.bauza.anna.futureworkshopsexercise.views;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by shmoolca on 24.11.2016.
 */
public class TweetViewHolder extends RecyclerView.ViewHolder  {
    private ViewDataBinding binding;

    public TweetViewHolder(View itemView) {
        super(itemView);
        binding = DataBindingUtil.bind(itemView);
    }

    public ViewDataBinding getBinding() {
        return binding;
    }
}
