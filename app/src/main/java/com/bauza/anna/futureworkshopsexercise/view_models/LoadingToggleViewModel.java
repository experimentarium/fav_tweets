package com.bauza.anna.futureworkshopsexercise.view_models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.bauza.anna.futureworkshopsexercise.BR;
import java.util.HashMap;

/**
 * Created by shmoolca on 24.11.2016.
 */



public class LoadingToggleViewModel  extends BaseObservable {

    HashMap<LoadingToggleState,Integer> drawables;
    private LoadingToggleState loadingToggleState;
    private String title;
    private Integer drawable;


    public LoadingToggleViewModel(LoadingToggleState state)
    {
        drawables = new HashMap<>();
        setLoadingToggleState(state);
    }

    @Bindable
    public LoadingToggleState getLoadingToggleState() {
        return loadingToggleState;
    }

    public void setLoadingToggleState(LoadingToggleState loadingToggleState)
    {
        this.loadingToggleState = loadingToggleState;
        notifyPropertyChanged(BR.loadingToggleState);
        reloadDrawable();
    }

    private void reloadDrawable()
    {
        if(drawables.containsKey(this.loadingToggleState))
            this.setDrawable( drawables.get(this.loadingToggleState));
        else
            this.setDrawable(null);
    }

    public void setDrawableForState(Integer drawable, LoadingToggleState state)
    {
        drawables.put(state,drawable);
        reloadDrawable();
    }

    @Bindable
    public Integer getDrawable() {
        return drawable;
    }

    public void setDrawable(Integer drawable) {
        this.drawable = drawable;
        notifyPropertyChanged(BR.drawable);
    }
}
