package com.bauza.anna.futureworkshopsexercise.views;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.bauza.anna.futureworkshopsexercise.R;
import com.bauza.anna.futureworkshopsexercise.helpers.DbHelper;
import com.bauza.anna.futureworkshopsexercise.models.FavouriteTweet;
import com.bauza.anna.futureworkshopsexercise.view_models.TweetViewModel;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.services.StatusesService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    TweetsAdapter adapter;
    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Realm.init(this);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.tweetsRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new TweetsAdapter();
        recyclerView.setAdapter(adapter);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                refreshItems();
            }
        });

        refreshItems();
    }

    void refreshItems() {
        if(this.isConnected())
            loadTweets();
        else
            loadFavourites();
        onItemsLoadComplete();
    }

    void onItemsLoadComplete() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                cleanAppData();
                startActivity(new Intent(this, LoginActivity.class));
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void  cleanAppData()
    {
        //clear database
        Realm realm = Realm.getDefaultInstance();
        realm.close();
        Realm.deleteRealm(realm.getConfiguration());
        //clear twitter session
        Twitter.getSessionManager().clearActiveSession();
        Twitter.logOut();
    }

    public void loadTweets() {
        Observable.create(new Observable.OnSubscribe<List<Tweet>>() {
            @Override
            public void call(Subscriber<? super List<Tweet>> subscriber) {
                final StatusesService service = Twitter.getInstance().getApiClient().getStatusesService();
                try {
                    subscriber.onNext(service.homeTimeline(200, null, null, null, null, null, null).execute().body());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        })
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Func1<List<Tweet>, ArrayList<TweetViewModel>>() {
                    public ArrayList<TweetViewModel> call(final List<Tweet> tweets) {
                        ArrayList<TweetViewModel> tweetViewModels = new ArrayList<TweetViewModel>();
                        for (Tweet tweet : tweets) {
                            TweetViewModel tweetViewModel = TweetViewModel.MAPPER.call(tweet);
                            DbHelper.syncFavoiurite(MainActivity.this,tweetViewModel);
                            tweetViewModels.add(tweetViewModel);
                        }
                        return tweetViewModels;
                    }
                })
                .subscribe(new Subscriber<List<TweetViewModel>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                    }

                    @Override
                    public void onNext(List<TweetViewModel> m) {
                        adapter.reloadAll(m);
                    }
                });
    }

    Subscription subscription;
    private void loadFavourites()
    {
        if(subscription!=null && subscription.isUnsubscribed())
            subscription.unsubscribe();
        final Realm realm =  Realm.getDefaultInstance();
        RealmResults<FavouriteTweet> favouriteTweetResults = realm.where(FavouriteTweet.class).findAllAsync();
        subscription =  favouriteTweetResults
                .asObservable()
                .subscribe(new Action1<RealmResults<FavouriteTweet>>() {
            @Override
            public void call(RealmResults<FavouriteTweet> favouriteTweets) {
                ArrayList<TweetViewModel> tweetViewModels = new ArrayList<>();
                for (FavouriteTweet ft:favouriteTweets) {
                    tweetViewModels.add( new TweetViewModel(ft.getTweet_id(),ft.getText(),ft.getProfile_picture(),ft.getUser_id(),true));
                }
                adapter.reloadAll(tweetViewModels);
                realm.close();
            }
        });
    }

    public boolean isConnected()
    {
        ConnectivityManager cm = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
      return (activeNetwork != null &&  activeNetwork.isConnectedOrConnecting());
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(subscription!=null && subscription.isUnsubscribed())
            subscription.unsubscribe();
    }
}
