package com.bauza.anna.futureworkshopsexercise.models;

import com.bauza.anna.futureworkshopsexercise.view_models.TweetViewModel;

import io.realm.RealmObject;

/**
 * Created by shmoolca on 24.11.2016.
 */
public class FavouriteTweet extends RealmObject {
    private long tweet_id;
    private long user_id;
    private String profile_picture;
    private String text;

    public void setTweetViewModel(TweetViewModel tweetViewModel)
    {
        setTweet_id(tweetViewModel.getTweet_id());
        setProfile_picture(tweetViewModel.getProfile_picture());
        setText(tweetViewModel.getText());
        setUser_id(tweetViewModel.getUser_id());
    }

    public long getTweet_id() {
        return tweet_id;
    }

    public void setTweet_id(long tweet_id) {
        this.tweet_id = tweet_id;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public String getProfile_picture() {
        return profile_picture;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
