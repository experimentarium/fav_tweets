package com.bauza.anna.futureworkshopsexercise.views;

import android.content.Context;
import android.content.Intent;
import android.databinding.BindingAdapter;
import android.net.Uri;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bauza.anna.futureworkshopsexercise.models.FavouriteTweet;
import com.bauza.anna.futureworkshopsexercise.networking.CustomService;
import com.bauza.anna.futureworkshopsexercise.networking.ExtendedTwitterApiClient;
import com.bauza.anna.futureworkshopsexercise.view_models.LoadingToggleState;
import com.bauza.anna.futureworkshopsexercise.view_models.TweetViewModel;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.models.Tweet;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by shmoolca on 24.11.2016.
 */
public class TweetViewBindingAdapter {


    @BindingAdapter("bind:openUserPage")
    public static void openUserPage(final ImageView imageView, final long user_id) {
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                String user_url = "https://twitter.com/intent/user?user_id="+user_id;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(user_url));
                context.startActivity(browserIntent);
            }
        });
    }

    @BindingAdapter("bind:toggleFavourite")
    public static void toggleFavourite(final ImageView imageView, final TweetViewModel tweetViewModel) {
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                if(tweetViewModel.getFavouriteToggleViewModel().getLoadingToggleState() == LoadingToggleState.Selected)
                    unfavourite(context, tweetViewModel);
                else if(tweetViewModel.getFavouriteToggleViewModel().getLoadingToggleState() == LoadingToggleState.Unselected)
                    favourite(context, tweetViewModel);
            }
        });
    }

    static void favourite(final Context context, final TweetViewModel tweetViewModel)
    {
        tweetViewModel.getFavouriteToggleViewModel().setLoadingToggleState(LoadingToggleState.Loading);
        final long time= System.currentTimeMillis();
        ExtendedTwitterApiClient client = new ExtendedTwitterApiClient(Twitter.getSessionManager().getActiveSession());
        CustomService stackApiService = client.getCustomService();
        Call<Tweet> fav_tweet =  stackApiService.favorite(tweetViewModel.getTweet_id());
        fav_tweet.enqueue(new Callback<Tweet>() {
            @Override
            public void onResponse(Call<Tweet> call, Response<Tweet> response) {
                if(response.isSuccessful()) {
                    Realm realm = Realm.getDefaultInstance();
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            FavouriteTweet favouriteTweet = realm.createObject(FavouriteTweet.class);
                            favouriteTweet.setTweetViewModel(tweetViewModel);

                            long delta_time= 6500 - (System.currentTimeMillis() -time);
                            if(delta_time<0 )
                                delta_time = 0;

                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                            tweetViewModel.getFavouriteToggleViewModel().setLoadingToggleState(LoadingToggleState.Selected);
                                }
                            }, delta_time);
                        }
                    });
                    realm.close();
                }
                else
                {
                    tweetViewModel.getFavouriteToggleViewModel().setLoadingToggleState(LoadingToggleState.Unselected);
                    Toast.makeText(context,"Could not finish action, please try again later",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Tweet> call, Throwable t) {
                t.printStackTrace();
                tweetViewModel.getFavouriteToggleViewModel().setLoadingToggleState(LoadingToggleState.Unselected);
                Toast.makeText(context,"Oops something went wrong",Toast.LENGTH_SHORT).show();
            }
        });
    }

    static void unfavourite(final Context context, final TweetViewModel tweetViewModel)
    {
        tweetViewModel.getFavouriteToggleViewModel().setLoadingToggleState(LoadingToggleState.Loading);
        final long time= System.currentTimeMillis();
        ExtendedTwitterApiClient client = new ExtendedTwitterApiClient(Twitter.getSessionManager().getActiveSession());
        CustomService stackApiService = client.getCustomService();
        final Call<Tweet> fav_tweet =  stackApiService.unfavorite(tweetViewModel.getTweet_id());
        fav_tweet.enqueue(new Callback<Tweet>() {
            @Override
            public void onResponse(Call<Tweet> call, Response<Tweet> response) {
                if(response.isSuccessful()) {
                    Realm realm = Realm.getDefaultInstance();

                    final FavouriteTweet favouriteTweet = realm.where(FavouriteTweet.class).equalTo("tweet_id", tweetViewModel.getTweet_id()).findFirst();
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            if(favouriteTweet != null)
                                favouriteTweet.deleteFromRealm();

                            long delta_time= 6500 - (System.currentTimeMillis() -time);
                            if(delta_time<0 )
                                delta_time = 0;

                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    tweetViewModel.getFavouriteToggleViewModel().setLoadingToggleState(LoadingToggleState.Unselected);
                                }
                            }, delta_time);

                        }
                    });
                    realm.close();
                }
                else
                {
                    tweetViewModel.getFavouriteToggleViewModel().setLoadingToggleState(LoadingToggleState.Selected);
                    Toast.makeText(context,"Could not finish action, please try again later",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Tweet> call, Throwable t) {
                t.printStackTrace();
                tweetViewModel.getFavouriteToggleViewModel().setLoadingToggleState(LoadingToggleState.Selected);
                Toast.makeText(context,"Oops something went wrong",Toast.LENGTH_SHORT).show();
            }
        });
    }

}
