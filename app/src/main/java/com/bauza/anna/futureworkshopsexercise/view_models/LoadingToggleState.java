package com.bauza.anna.futureworkshopsexercise.view_models;

public enum LoadingToggleState
{
    Selected,
    Unselected,
    Loading
}
