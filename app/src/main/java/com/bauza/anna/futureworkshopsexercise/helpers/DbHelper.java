package com.bauza.anna.futureworkshopsexercise.helpers;

import android.content.Context;

import com.bauza.anna.futureworkshopsexercise.models.FavouriteTweet;
import com.bauza.anna.futureworkshopsexercise.view_models.LoadingToggleState;
import com.bauza.anna.futureworkshopsexercise.view_models.TweetViewModel;

import io.realm.Realm;

/**
 * Created by shmoolca on 25.11.2016.
 */
public class DbHelper {

    public static void syncFavoiurite(Context context, final TweetViewModel tweetViewModel)
    {
        if(tweetViewModel.getFavouriteToggleViewModel().getLoadingToggleState() == LoadingToggleState.Selected)
        {
            Realm realm = Realm.getDefaultInstance();
            final FavouriteTweet favouriteTweet = realm.where(FavouriteTweet.class).equalTo("tweet_id", tweetViewModel.getTweet_id()).findFirst();
            if(favouriteTweet == null) {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        FavouriteTweet favouriteTweet = realm.createObject(FavouriteTweet.class);
                        favouriteTweet.setTweetViewModel(tweetViewModel);
                    }
                });
            }
            realm.close();
        }
        else if(tweetViewModel.getFavouriteToggleViewModel().getLoadingToggleState() == LoadingToggleState.Unselected)
        {
            Realm realm = Realm.getDefaultInstance();
            final FavouriteTweet favouriteTweet = realm.where(FavouriteTweet.class).equalTo("tweet_id", tweetViewModel.getTweet_id()).findFirst();
            if(favouriteTweet != null) {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        if (favouriteTweet != null)
                            favouriteTweet.deleteFromRealm();
                    }
                });
            }
            realm.close();
        }
    }
}
