package com.bauza.anna.futureworkshopsexercise.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by shmoolca on 24.11.2016.
 */
public class TwitterStatus {
    @SerializedName("id")
    private
    long id;

    public  TwitterStatus(long id)
    {
        setId(id);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
